=========
Biography
=========


Long ago
========

    - New Jersey
    - Madrid
    - Mojacar
    - Xalapa
    - Baltimore
    - Madrid
    - Buffalo
    - Mojacar
    - Sant Feliu de Guixols
    - Puerto Rico
    - New Haven
    - Buffalo

Then, California
================

    - Berkeley
    - Los Altos
    - Sausalito
    - Mountain View
    - Las Cumbres

Now
===

    - Massachusetts

