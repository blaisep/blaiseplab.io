========
Projects
========

Now showing
===========

    - `En Voz Alta <https://www.twitch.tv/envozalta>`_
    - `Cucumberbdd new contributors ensemble programming <https://youtu.be/906fZYsXatY>`_
    - `zero2datadog tutorial <https://zero2datadog.readthedocs.io/en/latest/>`_
    - `Vitrina <https://vitrina.readthedocs.io/>`_


Recent Hits
===========

    - Tech Doc Automation: using custom LaTeX libraries with Sphinx
    - Gearbox: an IOT network appliance (now: iotgearbox.com )
    - Visa & Master card EMV network tokenization as a service.





