============
Blaise Pabon
============

.. image:: ./_images/headshot.jpg
    :alt: 'portait of Blaise Pabon'
    :scale: 45
    :align: center

.. toctree::
    :glob:

    bio
    projects
    resume

Contact
=======

    - Twitter: controlpl4n3
    - home: blaise at gmail
    - fedora: blaise at fedoraproject.org



