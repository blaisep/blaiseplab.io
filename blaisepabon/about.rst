=====================
About __envozalta__
=====================



[Nikola:](https://getnikola.com/handbook.html#what-if-i-dont-want-a-blog)
Sirve de punto de partida para recursos y actividades.
Mantiene páginas principales
Documentación como código
publica las actualizaciones, eventos y apuntes

PlantUML
[diagramas como código](https://crashedmind.github.io/PlantUMLHitchhikersGuide/C4/C4Stdlib.html)

Markwhen
[Eventos como código](https://markwhen.com/)

Vitrina:
	applicación de demo
	DNS
	HTTP
	AuthN
	AuthZ
	TLS
	Cache de PyPi, Docker, RPM
