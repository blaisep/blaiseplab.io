# -*- coding: utf-8 -*-
import time


# Data about this site
BLOG_AUTHOR = "Blaise Pabon"  # (translatable)
BLOG_TITLE =  {"en": "About Blaise Pabon", "es": "Página principal de Blaise Pabón"}
# This is the main URL for your site.
SITE_URL = "https://blaisepabon.com"
# This is the URL where Nikola's output will be deployed.
# If not set, defaults to SITE_URL
BASE_URL = "https://blaisepabon.com/"
INDEX_PATH = "blog"  # To avoid a conflict because blogs try to generate /index.html

BLOG_EMAIL = "jitkelme@gmail.com"
BLOG_DESCRIPTION =  {"en": "Context and Musings", "es": "Datos y Esperpentos"}  # (translatable)
DEFAULT_LANG = "en"
TRANSLATIONS = {
    DEFAULT_LANG: "",
    "es": "./es",
}
TRANSLATIONS_PATTERN = '{path}.{lang}.{ext}'
NAVIGATION_LINKS = {
    DEFAULT_LANG: (
        ("/archive.html", "Archive"),
        ("/categories/", "Tags"),
        ("/rss.xml", "RSS feed"),
    ),
    "es": (
        ("/archive.html", "Archivos"),
        ("/categories/", "Categorias"),
        ("/rss.xml", "RSS"),
    )
}
NAVIGATION_ALT_LINKS = {
    DEFAULT_LANG: ()
}
THEME = "lanyon"
THEME_COLOR = '#5670d4'
POSTS = (
    ("posts/*.rst", "blog", "post.tmpl"),
    ("posts/*.txt", "blog", "post.tmpl"),
    ("posts/*.html", "blog", "post.tmpl"),
)

PAGES = (
    ("pages/*.rst", "pages", "page.tmpl"),
    ("pages/*.md", "pages", "page.tmpl"),
    ("pages/*.txt", "pages", "page.tmpl"),
    ("pages/*.html", "pages", "page.tmpl"),
)

TIMEZONE = "Etc/UTC"
COMPILERS = {
    "rest": ['.rst', '.txt'],
    "markdown": ['.md', '.mdown', '.markdown'],
    "textile": ['.textile'],
    "txt2tags": ['.t2t'],
    "wiki": ['.wiki'],
    "ipynb": ['.ipynb'],
    "html": ['.html', '.htm'],
}
HIDDEN_TAGS = ['mathjax']
CATEGORY_ALLOW_HIERARCHIES = False
CATEGORY_OUTPUT_FLAT_HIERARCHY = False
# Change to False when we get more than four posts in a category -Blaise
CATEGORY_PAGES_ARE_INDEXES = True


CATEGORY_DESCRIPTIONS = {
   DEFAULT_LANG: {
       "blogging": "Meta-blog posts about blogging.",
       "open source": "My contributions to my many, varied, ever-changing, and eternal libre software projects.",
        "OutLoud": "Working in public.",
   },
    "es": {
        "apuntes": "Un blog de temas según surgen.",
        "código libre": "Temas de open source y desarrollo en público",
        "envozalta": "Trabajando al descubierto.",
    }
}

# Set special titles for category pages. The default is "Posts about CATEGORY".
# (translatable)
CATEGORY_TITLES = {
   DEFAULT_LANG: {
       "blogging": "Meta-posts about blogging",
       "open source": "Posts about open source software"
   },
    "es": {
        "apuntes": "Sobre la marcha.",
        "código libre": "Temas de open source",
        "envozalta": "Trabajando al descubierto.",
    }
}

HIDDEN_CATEGORIES = []

# Author pages and links to them will still be generated.
HIDDEN_AUTHORS = ['Guest']
FRONT_INDEX_HEADER = {
    DEFAULT_LANG: ''
}

ATOM_FILENAME_BASE = "feed"
REDIRECTIONS = []
GITHUB_SOURCE_BRANCH = 'src'
GITHUB_DEPLOY_BRANCH = 'public'

# The name of the remote where you wish to push to, using github_deploy.
GITHUB_REMOTE_NAME = 'origin'
GITHUB_COMMIT_SOURCE = True
GALLERIES_USE_THUMBNAIL = False
GALLERIES_DEFAULT_THUMBNAIL = None
IMAGE_FOLDERS = {'images': 'images'}
#

# 'Read more...' for the index page, if INDEX_TEASERS is True (translatable)
INDEX_READ_MORE_LINK = '<p class="more"><a href="{link}">{read_more}…</a></p>'
# 'Read more...' for the feeds, if FEED_TEASERS is True (translatable)
FEED_READ_MORE_LINK = '<p><a href="{link}">{read_more}…</a> ({min_remaining_read})</p>'
FEED_LINKS_APPEND_QUERY = False
LICENSE = ""
# I recommend using the Creative Commons' wizard:
# https://creativecommons.org/choose/
# LICENSE = """
# <a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/">
# <img alt="Creative Commons License BY-NC-SA"
# style="border-width:0; margin-bottom:12px;"
# src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png"></a>"""

CONTENT_FOOTER = 'Contents &copy; {date}         <a href="mailto:{email}">{author}</a> - Powered by         <a href="https://getnikola.com" rel="nofollow">Nikola</a>         {license}'

CONTENT_FOOTER_FORMATS = {
    DEFAULT_LANG: (
        (),
        {
            "email": BLOG_EMAIL,
            "author": BLOG_AUTHOR,
            "date": time.gmtime().tm_year,
            "license": LICENSE
        }
    )
}


RSS_COPYRIGHT = 'Contents © {date} <a href="mailto:{email}">{author}</a> {license}'
RSS_COPYRIGHT_PLAIN = 'Contents © {date} {author} {license}'
RSS_COPYRIGHT_FORMATS = CONTENT_FOOTER_FORMATS
COMMENT_SYSTEM = ""
COMMENT_SYSTEM_ID = ""
STRIP_INDEXES = True
PRETTY_URLS = True
MARKDOWN_EXTENSIONS = ['markdown.extensions.fenced_code', 'markdown.extensions.codehilite', 'markdown.extensions.extra']
USE_BUNDLES = False
USE_TAG_METADATA = False
WARN_ABOUT_TAG_METADATA = False
GLOBAL_CONTEXT = {}
GLOBAL_CONTEXT_FILLER = []
